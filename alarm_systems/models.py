# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your models here.

from django.db import models

from taggit.managers import TaggableManager


class System(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

class Manufacturer(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class EquipmentType(models.Model):
    # TODO: merge type lists from all manufacturers
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Service(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Equipment(models.Model):
    name = models.CharField(max_length=200)
    part_number = models.CharField(max_length=200)
    description = models.TextField()
    system = models.ManyToManyField(System)
    manufacturer = models.ForeignKey(Manufacturer)
    panels = models.ManyToManyField('self', blank=True)
    equipment_type = models.ManyToManyField(EquipmentType)
    recommended_services = models.ManyToManyField(Service, blank=True)
    # TODO: equipment pairs that must be ordered separately instead of in kits

    tags = TaggableManager()

    class Meta:
        verbose_name_plural = 'equipment'


    def __unicode__(self):
        return self.name
    #TODO: partial search






