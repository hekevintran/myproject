from django.conf.urls import url

from alarm_systems.views import EquipmentListView

urlpatterns = [
    url(r'^equipment_list/', EquipmentListView.as_view(), name='equipment_list'),
]