# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from alarm_systems.models import Equipment
from alarm_systems.models import EquipmentType
from alarm_systems.models import Manufacturer
from alarm_systems.models import Service
from alarm_systems.models import System

admin.site.register(Equipment)
admin.site.register(EquipmentType)
admin.site.register(Manufacturer)
admin.site.register(Service)
admin.site.register(System)
