# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import ListView



# Create your views here.
from alarm_systems.models import Equipment


class EquipmentListView(ListView):
    template_name = 'equipment_list.html'
    model = Equipment

    def get_queryset(self):
        queryset = super(EquipmentListView, self).get_queryset()
        query = self.request.GET.get('query')
        if query:
            new_queryset = queryset.filter(name__icontains=query)
            return new_queryset
        return queryset
    #TODO: controlflow example for lesson 3