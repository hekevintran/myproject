from django.views.generic import TemplateView

panel_mapping = {
    'ms-2': {
        'name': 'MS-2',
        'image': 'images/ms-2-t.jpg',
        'description': 'MS-2 is a two zone conventional fire alarm control panel compatible with the new I3 smoke detectors from System Sensor, and provides such advanced features as drift compensation, maintenance alert, and freeze warning. Automatic synchronization of audio/visual devices is provided, using manufacturer protocol for System Sensor, Wheelock and Gentex appliances. The NAC protocol includes the ability to silence audible devices while strobes continue to flash, using only a single pair of wires.',
        'url': 'http://www.firelite.com/en-US/Pages/Product.aspx?category=ConventionalFACP&cat=HLS-FIRELITE&pid=ms-2',
    },
    'ms-4': {
        'name': 'MS-4',
        'image': 'images/ms-4-t.jpg',
        'description': 'MS-4 is a four zone conventional fire alarm control panel compatible with the new I3 smoke detectors from System Sensor, and provides such advanced features as drift compensation, maintenance alert, and freeze warning. Automatic synchronization of audio/visual devices is provided, using manufacturer protocol for System Sensor, Wheelock and Gentex appliances. The NAC protocol includes the ability to silence audible devices while strobes continue to flash, using only a single pair of wires.',
        'url': 'http://www.firelite.com/en-US/Pages/Product.aspx?category=ConventionalFACP&cat=HLS-FIRELITE&pid=ms-4',
    },
    'ms-5ud': {
        'name': 'MS-5UD',
        'image': 'images/ms-5ud-t.jpg',
        'description': 'MS-5UD-3 is a five zone conventional fire control panel has features normally found in more expensive addressable systems. The MS-5UD-3 is fully programmable via built-in keypad and 80 character LCD display, so you do not need an external programmer. The five zones panel has a built-in Digital Alarm Communicator Transmitter (DACT) and availability to add optional equipment: Remote annunciator, Relay module, LED Driver module, Transmitter module and Printer interface module.',
        'url': 'http://www.firelite.com/en-US/Pages/Product.aspx?category=ConventionalFACP&cat=HLS-FIRELITE&pid=ms-5ud',
    },
    'ms-10ud': {
        'name': 'MS-10UD',
        'image': 'images/ms-10ud-t.jpg',
        'description': 'MS-10UD-7 is a ten zone conventional fire control panel has features normally found in more expensive addressable systems. The MS-10UD-7 is fully programmable via built-in keypad and 80 character LCD display, so you do not need an external programmer. The ten zones panel has a built-in Digital Alarm Communicator Transmitter (DACT) and availability to add optional equipment: Remote annunciator, Relay module, LED Driver module, Transmitter module and Printer interface module.',
        'url': 'http://www.firelite.com/en-US/Pages/Product.aspx?category=ConventionalFACP&cat=HLS-FIRELITE&pid=ms-10ud',
    },
    'ms-9050ud': {
        'name': 'MS-9050UD',
        'image': 'images/ms-9050-ud-t.jpg',
        'description': 'MS-9050UD is a 50-point addressable fire alarm control panel ideal for smaller facilities such as apartment complexes, banks, cinemas, child care and elementary schools, department stores, food stores, places of worship, restaurants, small retail outlet stores, and other facilities requiring performance-based, sophisticated technology at reasonable cost.',
        'url': 'http://www.firelite.com/en-US/Pages/Product.aspx?category=Addressable%20Fire%20Alarm%20Control%20Panels&cat=HLS-FIRELITE&pid=ms-9050ud',
    },
    'ms-9200udls': {
        'name': 'MS-9200UDLS',
        'image': 'images/ms-9200udls-t.jpg',
        'description': 'MS-9200UDLS (Rev 3) is a 198-point addressable fire alarm control panel and Digital Alarm Communicator/Transmitter all on one circuit board. This latest version of the MS-9200UDLS includes removable terminal blocks, 2.0A of auxillary power, connection for second ANN BUS, synchronization of up to 16 NAC power supplies and improved protection against electronic shock damage.',
        'url': 'http://www.firelite.com/en-US/Pages/Product.aspx?category=Addressable%20Fire%20Alarm%20Control%20Panels&cat=HLS-FIRELITE&pid=ms-9200udls',
    },
    'ms-9600ls': {
        'name': 'MS-9600LS/MS-9600UDLS',
        'image': 'images/ms-9600ls-t.jpg',
        'description': 'MS-9600LS is a 318-point addressable fire alarm control panel with optional Digital Alarm Communicator/Transmitter (DACT-UD2) and expandable to 636 addressable points with an optional second Signaling Line Circuit (SLC-2LS).',
        'url': 'http://www.firelite.com/en-US/Pages/Product.aspx?category=Addressable%20Fire%20Alarm%20Control%20Panels&cat=HLS-FIRELITE&pid=ms-9600ls',
    },
}
for code, value in panel_mapping.items():
    value['code'] = code

result_mapping = {
    ('conventional', '10_or_fewer'): ['ms-2', 'ms-4', 'ms-5ud', 'ms-10ud'],
    ('conventional', '11_50'): [],
    ('conventional', '51_200'): [],
    ('conventional', '201_600'): [],
    ('addressable', '10_or_fewer'): ['ms-9050ud'],
    ('addressable', '11_50'): ['ms-9050ud'],
    ('addressable', '51_200'): ['ms-9200udls'],
    ('addressable', '201_600'): ['ms-9600ls'],
    ('hybrid', '10_or_fewer'): [],
    ('hybrid', '11_50'): [],
    ('hybrid', '51_200'): [],
    ('hybrid', '201_600'): [],
}


class FireSystemSurveyView(TemplateView):
    template_name = 'fire_system_survey.html'

    def get_context_data(self, **kwargs):
        context = super(FireSystemSurveyView, self).get_context_data()
        system = self.request.GET.get('system')
        zone_count = self.request.GET.get('zone_count')
        results = None
        if system and zone_count:
            results = result_mapping[(system, zone_count)]
            results = [panel_mapping[r] for r in results]
        context['system'] = system
        context['zone_count'] = zone_count
        context['results'] = results
        return context


class EquipmentCompatibilitySurveyView(TemplateView):
    template_name = 'equipment_compatibility_survey.html'

    def get_context_data(self, **kwargs):
        context = super(EquipmentCompatibilitySurveyView, self).get_context_data()
        panel = self.request.GET.get('panel')
        context['panel'] = panel_mapping[panel]
        return context
